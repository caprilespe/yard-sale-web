import React from 'react';
import '../styles/global.scss';

const App = () => {
    return (
        <h1>Hello World! 👋 🌎 in Reactjs ⚛️</h1>
    );
};

export default App;